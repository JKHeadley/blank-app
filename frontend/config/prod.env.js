module.exports = {
  NODE_ENV: '"production"',
  SERVER_URI: '"https://sandbox.api.appyapp.io"',
  WEBSOCKET_URI: '"wss://sandbox.api.appyapp.io"',
  APP_URI: '"https://sandbox.demo.appyapp.io"'
}
